function fairP=getPfair(fairP,S,songInSeconds)
step=round(length(fairP(1,:))/songInSeconds);
logS=real(log(S));

for second=1:step:(round(length(fairP(1,:)))-step)
    for thresholdeachsec = mean(mean(real(log(S(second:(second+step)))))):0.1:max(max(real(log(S(second:(second+step))))))
        count = 0;
        for i = 1:length(fairP(:,1))
            for j = second:(second+step)
                if(fairP(i,j)==1)
                    if(logS(i,j)< thresholdeachsec)
                        fairP(i,j) = 0;
                    else
                        count = count + 1;
                    end
                end
            end
        end

        if(30>=count)
            break;
        end
    end
end
end