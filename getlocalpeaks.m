function P=getlocalpeaks(gs,S)
CS = circshift(S, [0,0]);


P = ((S-CS)>0)+1;
% for i=-floor(gs/2):floor(gs/2)
%     for j=-floor(gs/2):floor(gs/2)
    for i=-gs:gs
        for j=-gs:gs
            if(i==j) 
                break;
            end
            CS = circshift(S, [i,j]);
            P = P.*((S-CS)>0);
        end
    end
end