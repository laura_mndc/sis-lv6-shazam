function pairs=maketable(title)
[y,fs] = audioread(title);
y=mean(y,2);
y=y-mean(y);
fs2=8000;
y = resample(y, fs2, fs); 
[S, F, T] = spectrogram(y, 512, 256, 512, fs2);
figure(1);
imagesc(T,F,real(log(S)));
xlabel('Vrijeme [t]');
ylabel('Frekvencija [Hz]');

P=getlocalpeaks(9,S);

figure(2);
colormap(1-gray);
imagesc(T,F,P);

songInSeconds = length(y)/8000;
numWantedPeaks = 30*round(songInSeconds);
logS=real(log(S));

fixedthresh=0.635;
figure(3);
Pfixed=getPfromfixed(P,fixedthresh,logS); %fiksiran prag, nadji P
colormap(1-gray);
imagesc(T,F,Pfixed);

Pnormal=getP(P,S,numWantedPeaks); %pronadji optimalan prag i P
figure(4);
colormap(1-gray);
imagesc(T,F,Pnormal);

fairP=getPfair(P,S,songInSeconds); %pronadji optimalan prag po sekundi i P
figure(5);
colormap(1-gray);
imagesc(T,F,fairP);

pairs=getpairs(fairP,F,T);

end
