function Pfixed=getPfromfixed(Pfixed,fixedthresh,logS)
for i = 1:length(Pfixed(:,1))
        for j = 1:length(Pfixed(1,:))
            if(Pfixed(i,j)==1)
                if(logS(i,j)< fixedthresh)
                    Pfixed(i,j) = 0;
                end
            end
        end
end
end