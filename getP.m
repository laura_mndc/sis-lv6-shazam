
function P=getP(P,S,numWantedPeaks)
logS=real(log(S));
for threshold = mean(mean(real(log(S)))):0.1:max(max(real(log(S))))
    count = 0;
    for i = 1:length(P(:,1))
        for j = 1:length(P(1,:))
            if(P(i,j)==1)
                if(logS(i,j)< threshold)
                    P(i,j) = 0;
                else
                    count = count + 1;
                end
            end
        end
    end
    
    if(numWantedPeaks>=count)
     
        break;
    end
end
end