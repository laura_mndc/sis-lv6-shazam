function formattedm=getpairs(peaks,F,T)
[i,j]=find(peaks);
fanout=3;
deltatl=10;
deltatu=40;
deltaf=12;
peaknum=length(i);
formattedm = zeros(fanout*peaknum, 4);
pozicije= zeros(fanout*peaknum, 4);
links=zeros(peaknum,1);
t1=1;

index=1;
for f1=1:peaknum %trenutni vrh 
    %links=0;
    t2=t1+1;
    for f2=(f1+1):peaknum %ostali vrhovi
        if((links(f1)>=fanout)||(links(f2)>=fanout))
            break; 
        end
        if((deltaf>=abs(i(f2)-i(f1))) && (j(t2)>j(t1)+deltatl) &&(j(t2)<=j(t1)+deltatu))
            %links=links+1;
            links(f1)=links(f1)+1;
            links(f2)=links(f2)+1;
            formattedm(index, :)=[F(i(f1)) F(i(f2)) T(j(t1)) T(j(t2))-T(j(t1))];
            pozicije(index, :)=[i(f1) i(f2) j(t1) j(t2)];
            index=index+1;
        end
       
        t2=t2+1;
    end
    t1=t1+1;
end
formattedm = formattedm(any(formattedm,2),:); %brise nulredove
figure(6)
    clf
    imagesc(peaks)
    axis xy;
    colormap(1-gray)
    hold on
for n = 1:size(pozicije,1)
        line([pozicije(n,3), pozicije(n,4)], [pozicije(n,1), pozicije(n,2)])

end
    
    xlabel('Time')
    ylabel('Frequency')
end